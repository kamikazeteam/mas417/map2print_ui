/*
*       A mapcontainer object for loading in and manipulating the openlayer
*       map containing a tile layer from Kartverket
*
*       Inital rendering of map is based on following blog post
*       https://taylor.callsen.me/using-openlayers-with-react-functional-components/
*       
*       and with help of this example from kartverket
*       https://github.com/kartverket/example-clients
*
*
*/

import React, { useEffect, useRef, useState } from "react";

import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import TileWMS from 'ol/source/TileWMS';
import { transform } from "ol/proj";
import VectorSource from 'ol/source/Vector'
import VectorLayer from 'ol/layer/Vector'
import Style from "ol/style/Style";
import Fill from "ol/style/Fill";
import {defaults} from 'ol/interaction';
import MapContext from "./MapContext";
import { toLonLat } from "ol/proj";


const MapContainer = ({ center, mapFeatures, onCenterView, fRef }) => {
    const [map, setMap] = useState()
    const [featuresLayer, setFeaturesLayer] = useState()
    const [selectedCoord , setSelectedCoord] = useState()

    const mapElement = useRef();
    const mapRef = useRef();
    mapRef.current = map;

    const fLayerRef = useRef();
    fLayerRef.current = featuresLayer;

    // Initiall rendering of map on component mount
    useEffect( () => {
        const url_ = "http://opencache.statkart.no/gatekeeper/gk/gk.open?";

        const center_ = [927207.6927416583, 8523726.832364988];

        const initalFeaturesLayer = new VectorLayer({
            source: new VectorSource(),
            style: new Style({
                fill: new Fill({
                    color: [0, 0, 255, 0.4],
                }),
            }),
        })
    
        // create map
        const initialMap = new Map({
            target: mapElement.current,
            layers: [
                // Kartverket WMS 
                new TileLayer({
                    source: new TileWMS({
                        url: url_,
                        preload: Infinity,
                        params: {
                            LAYERS: 'norges_grunnkart',
                            VERSION: '1.1.1',
                        }
                    })
                }),
                initalFeaturesLayer,
            ],
            interactions : defaults({doubleClickZoom :false}),
            view: new View({
                projection: 'EPSG:3857',
                center: center_,
                zoom: 7,
                minZoom: 5,
                maxZoom: 13,
                enableRotation: false,
            }),
            controls: []
        })
        // set map onclick handler
        initialMap.on('click', handleMapClick)

        setMap(initialMap)
        setFeaturesLayer(initalFeaturesLayer)
        setSelectedCoord(center_);

        const [lon, lat] = toLonLat(center_);
        center([
            lon.toString(),
            lat.toString()
        ]);

        // pass ref of feature layer up in order to get the excent of the drawn box
        fRef(fLayerRef);

    }, []);
    

    useEffect( () => {
        if (Array.isArray(mapFeatures) || !mapFeatures.length) {

            if(mapFeatures.geometryName_ === "geometry") {
               
                // set features to map
                featuresLayer.setSource(
                    new VectorSource({
                        features: [mapFeatures,],
                    }),
                )
            }
        }
    },[mapFeatures])

    
    useEffect( () => {
        if(!map) return;

        if((Array.isArray(mapFeatures) || !mapFeatures.length) && mapFeatures.geometryName_ === "geometry") {
            // fit map to feature extent (with 100px of padding)
            map.getView().fit(featuresLayer.getSource().getExtent(), {
                padding: [150,150,150,150]
            })
        } else {
            mapRef.current.getView().setCenter(selectedCoord);
        }
    }, [onCenterView])
      

        // map click handler
    const handleMapClick = (e) => {

        e.preventDefault();

        // get clicked coordinate using mapRef to access current React state inside OpenLayers callback
        //  https://stackoverflow.com/a/60643670
        const clickedCoord = mapRef.current.getCoordinateFromPixel(e.pixel);

        // transform coord to EPSG 4326 standard Lat Long
        const [lon, lat] = transform(clickedCoord, 'EPSG:3857', 'EPSG:4326');
        
        setSelectedCoord(clickedCoord);

        // update prop to display
        center([   
            lon.toString(), 
            lat.toString()
        ]);
    }

    

    return (
        <MapContext.Provider value={{ map }}>
            <div ref={mapElement} className="map-container" />
        </MapContext.Provider>
    )
}

export default MapContainer;


