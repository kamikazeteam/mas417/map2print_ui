/*
*       FileGenerator component which implements consists of the websocket client and
*       communication to the server. Downloading of the STL file also happens here
*
*       The websocket and its handlers are written with help from this blog post:
*       https://dev.to/bravemaster619/how-to-use-socket-io-client-correctly-in-react-app-o65
*
*/

import React, { useState, useContext, useCallback, useEffect } from 'react';
import { SocketContext } from './socket'
import { toLonLat } from "ol/proj";

const FileGenerator = ({ featureRef, mapFeature, heightScale }) => {
    const socket = useContext(SocketContext);

    const [infoMessage, setInfoMessage] = useState('');
    const [showGenBtn, setShowGenBtn] = useState(true);

    const infoMsgHandler = useCallback( (msg) => {
        setInfoMessage(msg);
    }, []);

    const downloadHandler = useCallback( () => {
        const link = document.createElement('a');
        link.href = "/get-stl";
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        
        setShowGenBtn(true);
    }, []);

    useEffect( () => {
        socket.on('download_stl', downloadHandler);
        socket.on('gen_msg', infoMsgHandler);

        return () => {
            socket.off('download_stl', downloadHandler);
            socket.off('gen_msg', infoMsgHandler);
        }

    }, [socket, downloadHandler, infoMsgHandler])


    const genereateSTL = () => {
        if(!Array.isArray(mapFeature) || mapFeature.geometryName_ === "geometry") {
          const box = featureRef.current.getSource().getExtent();
          const boxLonLat = [toLonLat([box[0], box[1]]), toLonLat([box[2], box[3]])]
          console.log(boxLonLat);
    
          socket.emit('generate', [boxLonLat[0][1],boxLonLat[0][0],boxLonLat[1][1],boxLonLat[1][0], heightScale])
          setShowGenBtn(false);
          setInfoMessage("Waiting for server...")
        } else {
          alert('Create box on map!')
        }
    }
    

    return (
        <div className="download">
            {showGenBtn ? (
                <input type='button' value='Generate & Download STL' className='act-button gen' onClick={genereateSTL} />
            ) : (
                <h2 className="infomsg" >{infoMessage}</h2>
            )}
        </div>
    )
}

export default FileGenerator;