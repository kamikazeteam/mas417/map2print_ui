/*
*       Component for the coordinate form and draw box buttons
*
*/

import React from "react";

const SetCoord = ({ lon, lat, onDraw, onCenterView }) => {
    

    return (
        <div className='form-control'>
            <label>Click map to set box-center coordinates</label>
            <input
                readOnly={true}
                type='text'
                placeholder='Longitude'
                value={lon ? lon : ''}
            />
            <input
                readOnly={true}
                type='text'
                placeholder='Latitude'
                value={lat ? lat : ''}
            />
            <div className='buttons' >
                <input type='button' value='Draw Box' className='act-button' onClick={onDraw} />
                <input type='button' value='Center View' className='act-button' onClick={onCenterView} />
            </div>
        </div>
    )
}

export default SetCoord