import React from "react"
import { io } from "socket.io-client"

export const socket = io({
    reconnection: true,
});
export const SocketContext = React.createContext();