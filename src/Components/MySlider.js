/*
*
*       Component for the sliders used in map2print.
*       Developed with the use of React library rc-slider from
*       https://github.com/react-component/slider
*
*
*
*/

import React, { useState, useEffect } from "react";
import Slider from "rc-slider";
import 'rc-slider/assets/index.css';

const MySlider = ({ label, unit, min, max, step, decimals, sliderVal }) => {
    const [value, setValue] = useState(min);

    const onSliderChange = (val) => {
        var factor = 1;

        switch(decimals) {
            case 1:
                factor = 10;
                break;
            case 2:
                factor = 100;
                break;
            case 3:
                factor = 1000;
                break;
            case 4:
                factor = 10000;
                break;
            default:
                break;
        }
        const value_ = Math.round(val*factor)/factor;
        setValue(value_);
        sliderVal(value_);
    }

    return (
        <div style={{ margin: 30 }}>
            <button className="slider-label">{label+value+unit}</button>
            <Slider style={{ marginTop: 10, display: "inline-block", float: "right", width: "58%" }}
                min={min}
                max={max}
                value={value}
                step={step}
                onChange={onSliderChange}
                railStyle={{
                    height: 10,
                    backgroundColor: "rgba(92, 89, 89, 0.8)",
                    boxShadow: "5px 5px 5px 5px rgba(0,0,0,0.3)",
                }}
                handleStyle={{
                    height: 24,
                    width: 24,
                    marginTop: -7,
                    marginLeft: 0,
                    backgroundColor: "rgb(70, 130, 180)",
                    border: 0,
                    boxShadow: "5px 5px 5px 5px rgba(0,0,0,0.3)"
                }}
                trackStyle={{
                    backgroundColor: "rgba(72, 8, 223, 0.9)",
                    height: 10,
                }}
            />
        </div>
    )
}

export default MySlider;