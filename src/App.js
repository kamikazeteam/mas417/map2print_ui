/*
*     Main application component which gets rendered directly on the React DOM
*     Includes all the subcomponents used in this program.
*
*     The websocket implementation was done with som help from this blog post:
*     https://dev.to/bravemaster619/how-to-use-socket-io-client-correctly-in-react-app-o65
*
*/
import React, { useState, useEffect } from "react";

import Header from './Components/Header';
import MapContainer from './Components/MapContainer';
import SetCoord from "./Components/SetCoord";
import MySlider from "./Components/MySlider";
import FileGenerator from "./Components/FileGenerator";

import Polygon from "ol/geom/Polygon";
import { Feature } from "ol";
import { fromLonLat } from "ol/proj";

import { SocketContext, socket } from "./Components/socket";


const App = () => {
  const [centerText, setCenterText] = useState([]);
  const [mapFeature, setMapFeature] = useState([]);
  const [centerView, setCenterView] = useState();
  
  const [boxWidth, setBoxWidth] = useState(0.02);
  const [boxHeight, setBoxHeight] = useState(0.01);
  const [boxScale, setBoxScale] = useState(1);
  const [boxFeature, setBoxFeature] = useState();
  const [featureRef, setFeatureRef] = useState();
  const [heightScale, setHeightScale] = useState(1);


  useEffect( () => {

    const center = [parseFloat(centerText[0]), parseFloat(centerText[1])]
    
    const lowerLeft = fromLonLat([center[0]-(boxWidth/2), center[1]-(boxHeight/2)]);
    const lowerRight = fromLonLat([center[0]+(boxWidth/2), center[1]-(boxHeight/2)]);
    const upperLeft = fromLonLat([center[0]-(boxWidth/2), center[1]+(boxHeight/2)]);
    const upperRight = fromLonLat([center[0]+(boxWidth/2), center[1]+(boxHeight/2)]);

    setBoxFeature(
      new Feature({
        geometry: new Polygon([
          [
            lowerLeft,
            lowerRight,
            upperRight,
            upperLeft,
            lowerLeft
          ]
        ]),
      })
    )

    if(!Array.isArray(mapFeature) || mapFeature.geometryName_ === "geometry") {
      setMapFeature(boxFeature);
    }

  }, [boxWidth, boxHeight, boxScale, centerText])

  
  return (
    <div className="main-container">
      <Header className="header" title="map2print" />
      <div className="row">
        <div className="column right">
          <div className="control-container">
            <SetCoord 
              onDraw={() => setMapFeature(boxFeature)}
              lon={centerText ? 'Longitude: '+'\xa0\xa0\xa0\xa0\xa0\xa0'+centerText[0]+'\u00B0' : ''}
              lat={centerText ? 'Latitude: '+'\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0'+centerText[1]+'\u00B0' : ''}
              onCenterView={() => setCenterView(centerView ? false : true)}
            />
            <MySlider label={'Height: '} unit={'\u00B0'} min={0.01} max={0.125*boxScale} step={0.001} decimals={4} sliderVal={(value) => setBoxHeight(value)}
            />
            <MySlider label={'Width: '} unit={'\u00B0'} min={0.02} max={0.25*boxScale} step={0.001} decimals={4} sliderVal={(value) => setBoxWidth(value)}
            />
            <MySlider label={'Box scale: '} unit={''} min={1} max={10} step={0.001} decimals={1} sliderVal={(value) => setBoxScale(value)}
            />
            <MySlider label={'Height scale: '} unit={''} min={1} max={4} step={0.01} decimals={2} sliderVal={(value) => setHeightScale(value)}
            />
            <SocketContext.Provider value={socket} >
              <FileGenerator featureRef={featureRef} mapFeature={mapFeature} heightScale={heightScale}/>
            </SocketContext.Provider>
          </div>
        </div>
        <div className="column left">
          <MapContainer
            fRef={(feRef) => setFeatureRef(feRef)}
            mapFeatures={mapFeature}
            center={(newCenter) => setCenterText(newCenter)}
            onCenterView={centerView}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
